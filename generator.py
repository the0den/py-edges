a = [1, 8, 15]
gen = (x for x in a if a.count(x) > 0)
a = [2, 8, 22]
print(list(gen))
# In a generator expression, the in clause is evaluated at declaration time,
# but the conditional clause is evaluated at runtime.
