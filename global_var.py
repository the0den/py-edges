l = [1, 2]


def append(e):
    l.append(e)
    return l


def add(e):
    l = l + [e]
    return l


try:
    print(append(3))
    # throws UnboundLocalError: local variable l referenced before assignment
    print(add(4))
except:
    print(l)
