# 3.6+


class A:
    val = 'A body'

    def __init_subclass__(cls):
        cls.val = 'A init subclass'


class B:
    def __init_subclass__(cls):
        cls.val = 'B init subclass'


class C(B, A):
    def __init_subclass__(cls):
        cls.val = 'C init subclass'

print(C.val)
# B init subclass, first in MRO
